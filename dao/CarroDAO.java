
package dao;

import classes.Carro;
import classes.Placa;
import enumeracoes.Querys;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import javax.management.Query;

/**
 *
 * @author Alex
 */
public class CarroDAO implements GenericDAO<Carro>{

  @Override
  public int insert(Carro obj) {
    int chavePrimaria = -1;
    try(Connection connection = new ConnectionFactory().getConnection();
      PreparedStatement stmt = 
              connection.prepareStatement(Querys.CARRO_INSERT.getSql(),
                        Statement.RETURN_GENERATED_KEYS)){
      System.out.println("Conex�o aberta!");
      stmt.setInt(1, obj.getAno());
      stmt.setString(2, obj.getModelo());
      stmt.setString(3, obj.getMontadora());
      stmt.execute();
      System.out.println("Dados Gravados!");
      ResultSet chaves = stmt.getGeneratedKeys();

      if (chaves.next())  chavePrimaria= chaves.getInt(1);

     }catch(SQLException e){
      System.out.println("Exce��o com recursos: "+e.getMessage());
    }catch(ClassNotFoundException e){
      System.out.println("Classe n�o encontrada!");
    }
    return chavePrimaria;
  }

  @Override
  public int update(Carro obj) {
    int count = 0;
    try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.PLACA_UPDATE.getSql())){
      stmt.setInt(1, obj.getAno());
      stmt.setString(2, obj.getModelo());
      stmt.setString(3, obj.getMontadora());
      stmt.execute();
      count = stmt.getUpdateCount();
    }catch(SQLException e){ 
      System.out.println("Exce��o SQL");
    }catch(Exception e){  
      System.out.println("Exce��o no c�digo!");
    }
    return count;
  }

  @Override
  public int delete(Carro obj) {
    int count = 0;
    try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.CARRO_DELETE.getSql())){
      stmt.execute();
      count = stmt.getUpdateCount();
    }catch(SQLException e){ 
      System.out.println("Exce��o SQL");
    }catch(Exception e){  
      System.out.println("Exce��o no c�digo!");
    }
    return count;
  }

  @Override
  public List<Carro> listAll() {
      List<Carro> lista = new LinkedList<>();

      try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.CARRO_LISTALL.getSql())){

          System.out.println("Conex�o aberta!");
          ResultSet rs = stmt.executeQuery();
          while(rs.next()){
              int ano = rs.getInt("ano");
              String modelo = rs.getString("modelo");
              String montadora = rs.getString("montadora");
              lista.add(new Carro(ano, modelo, montadora));
          }
          return lista;
      }catch(SQLException e){ 
        System.out.println("Exce��o SQL 1 "+Querys.CARRO_LISTALL.getSql());
      }catch(Exception e){  
        System.out.println("Exce��o no c�digo! 2");
      }
      return null;
  }

  @Override
  public Carro findByID(int id) {
      List<Carro> lista = new LinkedList<>();

      try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.CARRO_LIST_BY_ID.getSql())){

          System.out.println("Conex�o aberta!");
          stmt.setInt(1, id);
          ResultSet rs = stmt.executeQuery();
          while(rs.next()){
              int ano = rs.getInt("ano");
              String modelo = rs.getString("modelo");
              String montadora = rs.getString("montadora");
              lista.add(new Carro(ano, modelo, montadora));
          }
          return (Carro) lista;
      }catch(SQLException e){ 
        System.out.println("Exce��o SQL");
      }catch(Exception e){  
        System.out.println("Exce��o no c�digo!");
      }
      return null;
  }
  
}
