
package dao;

/**
 *
 * @author Silvia
 */
public interface GenericDAO <T> {
    public int insert(T obj);
    public int update(T obj);
    public int delete(T obj);
    public java.util.List<T> listAll();
    public T findByID(int id);
}

