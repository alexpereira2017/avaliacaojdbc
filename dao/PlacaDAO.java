
package dao;

import classes.Carro;
import classes.Placa;
import enumeracoes.Querys;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

/**
 *
 * @author Alex
 */
public class PlacaDAO  implements GenericDAO<Placa>{

  @Override
  public int insert(Placa obj) {
        int chavePrimaria = -1;
    try(Connection connection = new ConnectionFactory().getConnection();
    PreparedStatement stmt = 
            connection.prepareStatement(Querys.PLACA_INSERT.getSql(),
                      Statement.RETURN_GENERATED_KEYS)){
    System.out.println("Conex�o aberta!");
    stmt.setString(1, obj.getLetras());
    stmt.setInt(2, obj.getNumeros());
    stmt.execute();
    System.out.println("Dados Gravados!");
    ResultSet chaves = stmt.getGeneratedKeys();
    if (chaves.next())  chavePrimaria= chaves.getInt(1);
    }catch(SQLException e){

      System.out.println("Exce��o com recursos: "+e.getMessage());
    }catch(ClassNotFoundException e){
      System.out.println("Classe n�o encontrada!");
    }
    return chavePrimaria;
  }

  @Override
  public int update(Placa obj) {
    int count = 0;
    try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.PLACA_UPDATE.getSql())){
      stmt.setString(1, obj.getLetras());
      stmt.setInt(2, obj.getNumeros());
      stmt.execute();
      count = stmt.getUpdateCount();
    }catch(SQLException e){ 
      System.out.println("Exce��o SQL");
    }catch(Exception e){  
      System.out.println("Exce��o no c�digo!");
    }
    return count;
  }

  @Override
  public int delete(Placa obj) {
    int count = 0;
    try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.PLACA_DELETE.getSql())){
      stmt.execute();
      count = stmt.getUpdateCount();
    }catch(SQLException e){ 
      System.out.println("Exce��o SQL");
    }catch(Exception e){  
      System.out.println("Exce��o no c�digo!");
    }
    return count;
  }

  @Override
  public List<Placa> listAll() {
      List<Placa> lista = new LinkedList<>();

      try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.PLACA_LISTALL.getSql())){

          System.out.println("Conex�o aberta!");
          ResultSet rs = stmt.executeQuery();
          while(rs.next()){
              String letras = rs.getString("letras");
              int numeros = rs.getInt("numeros");
              lista.add(new Placa(letras, numeros));
          }
          return lista;
      }catch(SQLException e){ 
        System.out.println("Exce��o SQL");
      }catch(Exception e){  
        System.out.println("Exce��o no c�digo!");
      }
      return null;
  }

  @Override
  public Placa findByID(int id) {
      List<Placa> lista = new LinkedList<>();

      try(Connection connection = new ConnectionFactory().getConnection();
          PreparedStatement stmt = 
                  connection.prepareStatement(Querys.PLACA_LIST_BY_ID.getSql())){

          System.out.println("Conex�o aberta!");
          stmt.setInt(1, id);
          ResultSet rs = stmt.executeQuery();
          while(rs.next()){
              String letras = rs.getString("letras");
              int numeros = rs.getInt("numeros");
              lista.add(new Placa(letras, numeros));
          }
          return (Placa) lista;
      }catch(SQLException e){ 
        System.out.println("Exce��o SQL");
      }catch(Exception e){  
        System.out.println("Exce��o no c�digo!");
      }
      return null;
  }
  
}
