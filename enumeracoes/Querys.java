
package enumeracoes;

/**
 *
 * @author Alex
 */
public enum Querys {

    CARRO_INSERT("INSERT INTO carro (ano,modelo,montadora) VALUES (?,?,?)"), 
    CARRO_LISTALL("SELECT * FROM carro"),
    CARRO_LIST_BY_ID("SELECT * FROM carro WHERE id = ?"),
    CARRO_DELETE("DELETE FROM carro WHERE id = ?"),
    CARRO_UPDATE("UPDATE carro SET ano = ?, modelo = ?, montadora = ?"),

    PLACA_INSERT("INSERT INTO placa (letras,numeros) VALUES (?,?)"), 
    PLACA_LISTALL("SELECT * FROM placa"),
    PLACA_LIST_BY_ID("SELECT * FROM placa WHERE id = ?"),
    PLACA_DELETE("DELETE FROM placa WHERE id = ?"),
    PLACA_UPDATE("UPDATE placa SET letras = '?', numeros = ?"),
    
    ;
    
    
    private final String sql;

    Querys(String sql){
        this.sql = sql; 
    
    }

    public String getSql() {
        return sql;
    }    

}
