/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import classes.Carro;
import classes.Placa;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Silvia
 */
public class MenuSimples {
    public static void main(String[] args) {
      Carro carro = new Carro();
      Placa placa = new Placa();
      
      List<Carro> listaCarros;
      List<Placa> listaPlacas ;
      
      while(true){
            switch(montaMenu()){
                case 1: 
                      menuCadastraCarro();
                      break;
                case 2: 
                      menuCadastraPlaca();
                      break;
                case 3: 
                      listaCarros = carro.listAll();
                      Collections.sort((List) listaCarros);

                      for (int i = 0; i < listaCarros.size(); i++) {
                        System.out.println(listaCarros.get(i).toString());
                      }
                      break;
                case 4: 
                      listaPlacas = placa.listAll();
                      for (int i = 0; i < listaPlacas.size(); i++) {
                        System.out.println(listaPlacas.get(i).toString());
                      }
                      break;
                case 5: 
                      System.exit(0);
                      break;
                default:
                    
            }
        }
    }
    private static int montaMenu(){
        //Scanner in = new Scanner(System.in);
        String menu = "Menu\n";
        menu += "1 Cadastrar Carro | ";
        menu += "2 Cadastrar Placa | ";
        menu += "3 Listar Carro | ";
        menu += "4 Listar Placa | ";
        menu += "5 - Sair";
        
        return Integer.parseInt(JOptionPane.showInputDialog(menu));
    }
    
    private static void menuCadastraCarro() {
      String obs = "Digite o ano";
      int ano = Integer.parseInt(JOptionPane.showInputDialog(obs));
      obs = "Digite o modelo";
      String modelo = (JOptionPane.showInputDialog(obs));
      obs = "Digite a montadora";
      String montadora = (JOptionPane.showInputDialog(obs));
      Carro carro = new Carro(ano, modelo, montadora);
      carro.insert();
    }
    
    private static void menuCadastraPlaca() {
      String obs = "Digite as 3 letras";
      String letras = (JOptionPane.showInputDialog(obs));
      
      obs = "Digite os 4 numeros";
      int numeros = Integer.parseInt(JOptionPane.showInputDialog(obs));
      Placa placa = new Placa(letras, numeros);
      placa.insert();
    }
}


