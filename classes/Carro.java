
package classes;

import dao.CarroDAO;
import java.util.List;

/**
 *
 * @author Alex
 */
public class Carro implements Comparable<Carro> {
  private int ano;
  private String modelo;
  private String montadora;
  private Placa placa;

  public Carro() {
    
  }

  public Carro(int ano, String modelo, String montadora) {
    this.ano = ano;
    this.modelo = modelo;
    this.montadora = montadora;
  }

  public int getAno() {
    return ano;
  }

  public void setAno(int ano) {
    this.ano = ano;
  }

  public String getModelo() {
    return modelo;
  }

  public void setModelo(String modelo) {
    this.modelo = modelo;
  }

  public String getMontadora() {
    return montadora;
  }

  public void setMontadora(String montadora) {
    this.montadora = montadora;
  }

  public Placa getPlaca() {
    return placa;
  }

  public void setPlaca(Placa placa) {
    this.placa = placa;
  }

  @Override
  public String toString() {
    return "Carro{" + "ano=" + ano + ", modelo=" + modelo + ", montadora=" + montadora + '}';
  }
  
  public int insert() {
      return new CarroDAO().insert(this);
  }
  
  public int update(Carro obj) {
    return new CarroDAO().update(obj);
  }
  
  public int delete(Carro obj) {
    return new CarroDAO().delete(obj);
  }
  
  public List<Carro> listAll() {
    return new CarroDAO().listAll();
  }
  
  public Carro findByID(int id) {
    return new CarroDAO().findByID(id);
  }

  @Override
  public int compareTo(Carro o) {
    if(getAno() == o.getAno())
        return 0;
    else if(getAno() < o.getAno())
        return 1;
    else return -1;
  }

}
