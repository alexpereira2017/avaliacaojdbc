package classes;

import dao.PlacaDAO;
import java.util.List;

/**
 *
 * @author Alex
 */
public class Placa {
  private String letras;
  private int numeros;

  public Placa() {}
  public Placa(String letras, int numeros) {
    this.letras = letras;
    this.numeros = numeros;
  }

  public String getLetras() {
    return letras;
  }

  public void setLetras(String letras) {
    this.letras = letras;
  }

  public int getNumeros() {
    return numeros;
  }

  public void setNumeros(int numeros) {
    this.numeros = numeros;
  }

  @Override
  public String toString() {
    return "Placa{" + "letras=" + letras + ", numeros=" + numeros + '}';
  }
  
  public int insert() {
      return new PlacaDAO().insert(this);
  }
  
  public int update(Placa obj) {
    return new PlacaDAO().update(obj);
  }
  
  public int delete(Placa obj) {
    return new PlacaDAO().delete(obj);
  }
  
  public List<Placa> listAll() {
    return new PlacaDAO().listAll();
  }
  
  public Placa findByID(int id) {
    return new PlacaDAO().findByID(id);
  }

  
}
